﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Information
{
    public class Country
    {
        public string Name { private set; get; }
        public uint Population { private set; get; }
        public float GDP { private set; get; }
        public short Continent { private set; get; }
        public short Side { private set; get; }

        public Country()
        {
            Name = "None";
            Population = 0;
            GDP = 0;
            Continent = 0;
            Side = 0;
        }

        public Country(string name)
        {
            Name = name;
            Population = 0;
            GDP = 0;
            Continent = 0;
            Side = 0;
        }
        public Country(string name, uint population, float gdp, short continent, short side)
        {
            Name = name;
            Population = population;
            GDP = gdp;
            Continent = continent;
            Side = side;
        }
        public Country(Country country)
        {
            Name = country.Name;
            Population = country.Population;
            GDP = country.GDP;
            Continent = country.Continent;
            Side = country.Side;
        }

        public static List<Country> GetCountries(BinaryReader reader)
        {
            try
            {
                int count = reader.ReadInt32();
                if (count == 0)
                {
                    reader.Close();
                    return new List<Country>();
                }
                List<Country> countries = new List<Country>();
                for (int i = 0; i < count; i++)
                {
                    string name = reader.ReadString();
                    uint population = reader.ReadUInt32();
                    float gdp = reader.ReadSingle();
                    short continent = reader.ReadInt16();
                    short side = reader.ReadInt16();
                    countries.Add(new Country(name, population, gdp,
                        continent, side));
                }
                reader.Close();
                return countries;
            }
            catch (EndOfStreamException)
            {
                reader.Close();
                return new List<Country>();
            }
        }
        public static void WriteCountries(BinaryWriter writer, List<Country> countries)
        {
            writer.Write(countries.Count);
            foreach(Country country in countries)
            {
                writer.Write(country.Name);
                writer.Write(country.Population);
                writer.Write(country.GDP);
                writer.Write(country.Continent);
                writer.Write(country.Side);
            }
            writer.Flush();
            writer.Close();
        }

        public static bool Compare(Country countryFirst, Country countrySecond)
        {
            if(countryFirst.Name == countrySecond.Name)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool CheckCountry(Country country)
        {
            Regex regex = new Regex(@"^[АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЮЯ][абвгґдеєжзиіїйклмнопрстуфхцчшщьюя\']+(?:[ \-,](?:(?:та|і|й)[ \-])?(?:[АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЮЯабвгґдеєжзиіїйклмнопрстуфхцчшщьюя]?[абвгґдеєжзиіїйклмнопрстуфхцчшщьюя\']*['\-]?)*)*$");
            Match match = regex.Match(country.Name);
            if(!match.Success)
            {
                return false;
            }
            return true;
        }
    }
}
