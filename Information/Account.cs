﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Information
{
    public class Account
    {
        static void Main()
        {

        }
        public string Login { private set; get; }
        public string Password { private set; get; }

        public Account()
        {
            Login = "None";
            Password = "None";
        }
        public Account(string login)
        {
            Login = login;
            Password = "None";
        }
        public Account(string login, string password)
        {
            Login = login;
            Password = password;
        }
        public Account(Account account)
        {
            Login = account.Login;
            Password = account.Password;
        }

        static public List<Account> GetAccounts(BinaryReader reader)
        {
            int count;
            try
            {
                count = reader.ReadInt32();
            }
            catch (Exception)
            {
                count = 0;
                return null;
            }
            List<Account> accounts = new List<Account>();
            for (int i = 0; i < count; i++)
            {
                string login = reader.ReadString();
                string password = reader.ReadString();
                accounts.Add(new Account(login, password));
            }
            return accounts;
        }

        static public bool CheckSignIn(List<Account> accounts, string login,
            string password, string repPassword, out string error)
        {
            bool flag = true;
            error = null;

            Regex regex = new Regex(@"^[A-Za-z0-9]+(?:[_\-.][A-Za-z0-9]+)*$");
            Match match = regex.Match(login);
            if (!match.Success)
            {
                flag = false;
                error = "Логін не має мати інших символів, окрім латинських літер, цифер, одного з символів: '-', '_', '.' " +
                    "- як роздільники (1 символ), а також не коротше 5 символів.";
            }
            else
            {
                if (accounts != null)
                {
                    foreach (Account account in accounts)
                    {
                        if (login == account.Login)
                        {
                            error = "Такий логін вже існує.";
                            flag = false;
                            break;
                        }
                    }
                }
            }
            regex = new Regex(@"^[A-Za-z\d_\-]{6,}$");
            match = regex.Match(password);
            if (!match.Success)
            {
                flag = false;
                if (error == null)
                {
                    error = "Пароль не має мати інших символів, окрім латинських літер, цифер, '_' та '-', " +
                    "а також не коротше 6 символів.";
                }
                else
                {
                    error += "\n\nПароль не має мати інших символів, окрім латинських літер, цифер, '_' та '-', " +
                    "а також не коротше 6 символів.";
                }
            }
            else
            {
                if (repPassword != password)
                {
                    flag = false;
                    if (error == null)
                    {
                        error = "Паролі мають співпадати.";
                    }
                    else
                    {
                        error += "\n\nПаролі мають співпадати.";
                    }
                }
            }
            return flag;
        }

        static public bool CheckLogIn(List<Account> accounts, string logIn, string password, out string error, out bool admin,
            out Account outAccount)
        {
            admin = false;
            outAccount = null;
            bool correct = true;
            error = null;

            Regex regex = new Regex(@"^[A-Za-z0-9]+(?:[_\-.][A-Za-z0-9]+)*$");
            Match match = regex.Match(logIn);
            if (!match.Success)
            {
                correct = false;
                error = "Логін не має мати інших символів, окрім латинських літер, цифер, одного з символів: '-', '_', '.' " +
                    "- як роздільники (1 символ), а також не коротше 5 символів.";
            }
            regex = new Regex(@"^[A-Za-z\d_\-]{6,}$");
            match = regex.Match(password);
            if (!match.Success)
            {
                correct = false;
                if (error != null)
                {
                    error += "\n\nПароль не має мати інших символів, окрім латинських літер, цифер, '_' та '-', " +
                    "а також не коротше 6 символів.";
                }
                else
                {
                    error = "Пароль не має мати інших символів, окрім латинських літер, цифер, '_' та '-', " +
                    "а також не коротше 6 символів.";
                }
            }

            if (error == null)
            {
                foreach (Account account in accounts)
                {
                    if (account.Login == logIn && account.Password == password)
                    {
                        admin = true;
                        outAccount = account;
                        correct = true;
                        break;
                    }
                    else
                    {
                        error = "Невірний логін чи пароль.";
                        correct = false;
                    }
                }
            }
            return correct;
        }
        static public void WriteAccounts(BinaryWriter writer, List<Account> accounts)
        {
            writer.Write(accounts.Count);
            foreach (Account account in accounts)
            {
                writer.Write(account.Login);
                writer.Write(account.Password);
            }
            writer.Flush();
        }
    }
}
