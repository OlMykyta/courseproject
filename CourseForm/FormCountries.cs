﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Information;

namespace CourseForm
{
    public partial class CountriesForm : Form
    {
        private bool Admin = false;
        private string AccountsPath = @".\Data\Accounts.bin";
        private string CountriesPath = @".\Data\Countries.bin";
        private List<Country> Countries;

        public void SetCountries(List<Country> countries)
        {
            Countries = countries;
            GridRefresh();
        }

        public void SetAccount(bool admin, Account Account)
        {
            Admin = admin;
            labelAccount.Text = $"Вітаємо, {Account.Login}";
            loginToolStripMenuItem.Text = "Вийти";
            labelAccount.Left = Width - 28 - labelAccount.Width;
            addToolStripMenuItem.Visible = updateToolStripMenuItem.Visible = deleteToolStripMenuItem.Visible = true;
            contextMenuStrip.Enabled = true;
            contextAddToolStripMenuItem.Enabled = true;
            contextUpdateToolStripMenuItem.Enabled = true;
            contextDeleteToolStripMenuItem.Enabled = true;
        }

        public CountriesForm()
        {
            InitializeComponent();

        }

        private void loginToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if (Admin == false)
            {
                AdminLogIn admin = new AdminLogIn(this, AccountsPath);
                admin.Show();
            }
            else
            {
                Admin = false;
                labelAccount.Text = "Адміністративні функції вимкнені";
                loginToolStripMenuItem.Text = "Увійти";
                labelAccount.Left = Width - 28 - labelAccount.Width;
                addToolStripMenuItem.Visible = updateToolStripMenuItem.Visible = deleteToolStripMenuItem.Visible = false;
                contextMenuStrip.Enabled = false;
                contextAddToolStripMenuItem.Enabled = false;
                contextUpdateToolStripMenuItem.Enabled = false;
                contextDeleteToolStripMenuItem.Enabled = false;
            }
        }

        private void generalModeToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            continentToolStripComboBox.SelectedIndex = 0;
            areaToolStripComboBox.SelectedIndex = 0;
            aimToolStripComboBox.SelectedIndex = 0;
            toolStripTextBox.Text = "";
            if (!generalModeToolStripMenuItem.Checked)
            {
                areaToolStripComboBox.SelectedIndex = 0;
                continentToolStripComboBox.SelectedIndex = 0;

                generalModeToolStripMenuItem.Checked = true;
                aimModeToolStripMenuItem.Checked = mixedModeToolStripMenuItem.Checked = false;
                toolStripSeparator.Visible = true;

                areaToolStripMenuItem.Visible = true;
                continentToolStripMenuItem.Visible = true;
                aimParametersToolStripMenuItem.Visible = false;
            }
            else
            {
                generalModeToolStripMenuItem.Checked = false;
                toolStripSeparator.Visible = false;
                areaToolStripMenuItem.Visible = false;

                areaToolStripMenuItem.Visible = false;
                continentToolStripMenuItem.Visible = false;
            }
            GridRefresh();
        }

        private void aimModeToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            continentToolStripComboBox.SelectedIndex = 0;
            areaToolStripComboBox.SelectedIndex = 0;
            aimToolStripComboBox.SelectedIndex = 0;
            toolStripTextBox.Text = "";
            if (!aimModeToolStripMenuItem.Checked)
            {
                aimToolStripComboBox.SelectedIndex = 0;
                aimModeToolStripMenuItem.Checked = true;
                generalModeToolStripMenuItem.Checked = mixedModeToolStripMenuItem.Checked = false;
                toolStripSeparator.Visible = true;

                areaToolStripMenuItem.Visible = false;
                continentToolStripMenuItem.Visible = false;
                aimParametersToolStripMenuItem.Visible = true;
            }
            else
            {
                aimModeToolStripMenuItem.Checked = false;
                toolStripSeparator.Visible = false;
                aimParametersToolStripMenuItem.Visible = false;
            }
            GridRefresh();
        }

        private void mixedModeToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            continentToolStripComboBox.SelectedIndex = 0;
            areaToolStripComboBox.SelectedIndex = 0;
            aimToolStripComboBox.SelectedIndex = 0;
            toolStripTextBox.Text = "";
            if (!mixedModeToolStripMenuItem.Checked)
            {
                areaToolStripComboBox.SelectedIndex = 0;
                continentToolStripComboBox.SelectedIndex = 0;
                aimToolStripComboBox.SelectedIndex = 0;

                generalModeToolStripMenuItem.Checked = aimModeToolStripMenuItem.Checked = false;
                mixedModeToolStripMenuItem.Checked = true;
                toolStripSeparator.Visible = true;

                areaToolStripMenuItem.Visible = true;
                continentToolStripMenuItem.Visible = true;
                aimParametersToolStripMenuItem.Visible = true;
            }
            else
            {
                mixedModeToolStripMenuItem.Checked = false;
                toolStripSeparator.Visible = false;

                areaToolStripMenuItem.Visible = false;
                continentToolStripMenuItem.Visible = false;
                aimParametersToolStripMenuItem.Visible = false;
            }
            GridRefresh();
        }

        private void FormCountries_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (!Directory.Exists(CountriesPath.Substring(0, CountriesPath.LastIndexOf('\\'))))
                {
                    Directory.CreateDirectory(CountriesPath.Substring(0, CountriesPath.LastIndexOf('\\')));
                }
                using (BinaryReader reader = new BinaryReader(File.Open(CountriesPath, FileMode.OpenOrCreate)))
                {
                    SetCountries(Country.GetCountries(reader));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GridRefresh()
        {
            dataGridViewCountries.RowCount = 0;
            if (!generalModeToolStripMenuItem.Checked &&
                !aimModeToolStripMenuItem.Checked &&
                !mixedModeToolStripMenuItem.Checked)
            {
                for (int i = 0; i < Countries.Count; i++)
                {
                    FillGridRow(Countries[i]);
                }
            }
            else if (generalModeToolStripMenuItem.Checked)
            {
                for(int i = 0; i < Countries.Count; i++)
                {
                    if((areaToolStripComboBox.SelectedIndex == 0 || 
                        areaToolStripComboBox.SelectedIndex == Countries[i].Side + 1) &&
                        (continentToolStripComboBox.SelectedIndex==0 ||
                        continentToolStripComboBox.SelectedIndex == Countries[i].Continent + 1))
                    {
                        FillGridRow(Countries[i]);
                    }
                }
            }
            else if (aimModeToolStripMenuItem.Checked)
            {
                for(int i = 0; i < Countries.Count; i++)
                {
                    if (AimMode(aimToolStripComboBox.SelectedIndex, toolStripTextBox.Text, Countries[i]))
                    {
                        FillGridRow(Countries[i]);
                    }
                }
            }
            else if (mixedModeToolStripMenuItem.Checked)
            {
                for(int i = 0; i < Countries.Count; i++)
                {
                    if((areaToolStripComboBox.SelectedIndex == 0 || areaToolStripComboBox.SelectedIndex == Countries[i].Side + 1) &&
                        (continentToolStripComboBox.SelectedIndex == 0 || continentToolStripComboBox.SelectedIndex == Countries[i].Continent + 1) &&
                        AimMode(aimToolStripComboBox.SelectedIndex, toolStripTextBox.Text, Countries[i]))
                    {
                        FillGridRow(Countries[i]);
                    }
                }
            }
            labelCounter.Text = Countries.Count.ToString();
        }

        private bool AimMode(int Mode, string Parameter, Country country)
        {
            if(Parameter.Length == 0)
            {
                return true;
            }
            if(Mode == 0)
            {
                if (country.Name.Contains(Parameter))
                {
                    return true;
                }
            }
            else
            {
                if(Mode == 1 || Mode == 2)
                {
                    int n;
                    if(!int.TryParse(Parameter, out n))
                    {
                        MessageBox.Show("Введіть ціле число");
                        toolStripTextBox.Text = "";
                        return false;
                    }
                    if(Mode == 1 && country.Population >= n)
                    {
                        return true;
                    }
                    if(Mode == 2 && country.Population <= n)
                    {
                        return true;
                    }
                }
                else
                {
                    float d;
                    if(!float.TryParse(Parameter, out d))
                    {
                        MessageBox.Show("Введіть дробове число");
                        toolStripTextBox.Text = "";
                        return false;
                    }
                    if(Mode == 3 && country.GDP >= d)
                    {
                        return true;
                    }
                    if(Mode == 4 && country.GDP <= d)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void FillGridRow(Country country)
        {
            int row = dataGridViewCountries.RowCount;
            dataGridViewCountries.RowCount += 1;
            dataGridViewCountries.Rows[row].Cells[0].Value = country.Name;
            dataGridViewCountries.Rows[row].Cells[1].Value = country.Population;
            dataGridViewCountries.Rows[row].Cells[2].Value = country.GDP;
            switch (country.Continent)
            {
                case 0:
                    dataGridViewCountries.Rows[row].Cells[3].Value = "Європа";
                    break;
                case 1:
                    dataGridViewCountries.Rows[row].Cells[3].Value = "Азія";
                    break;
                case 2:
                    dataGridViewCountries.Rows[row].Cells[3].Value = "Африка";
                    break;
                case 3:
                    dataGridViewCountries.Rows[row].Cells[3].Value = "Північна Америка";
                    break;
                case 4:
                    dataGridViewCountries.Rows[row].Cells[3].Value = "Південная Америка";
                    break;
                case 5:
                    dataGridViewCountries.Rows[row].Cells[3].Value = "Австралія та Океанія";
                    break;
                default:
                    break;
            }
            switch (country.Side)
            {
                case 0:
                    dataGridViewCountries.Rows[row].Cells[4].Value = "Північ";
                    break;
                case 1:
                    dataGridViewCountries.Rows[row].Cells[4].Value = "Південь";
                    break;
                case 2:
                    dataGridViewCountries.Rows[row].Cells[4].Value = "Захід";
                    break;
                case 3:
                    dataGridViewCountries.Rows[row].Cells[4].Value = "Схід";
                    break;
                case 4:
                    dataGridViewCountries.Rows[row].Cells[4].Value = "Центр";
                    break;
                default:
                    break;
            }
        }

        private void AddCountry()
        {
            CreateForm createForm = new CreateForm(this, CountriesPath);
            createForm.Text = "Створити країну";
            createForm.Show();
        }

        private void addToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            AddCountry();
        }

        private void contextAddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddCountry();
        }

        private void UpdateCountry()
        {
            if (Countries != null)
            {
                if (dataGridViewCountries.SelectedRows.Count > 0)
                {
                    foreach (Country country in Countries)
                    {
                        if (country.Name == dataGridViewCountries.SelectedRows[0].Cells[0].Value.ToString())
                        {
                            CreateForm createForm = new CreateForm(this, country, CountriesPath);
                            createForm.Text = "Редагувати країну";
                            createForm.Show();
                        }
                    }
                }
                else if (dataGridViewCountries.SelectedCells.Count > 0)
                {
                    int row = dataGridViewCountries.SelectedCells[0].RowIndex;
                    foreach (Country country in Countries)
                    {
                        if (country.Name == dataGridViewCountries.Rows[row].Cells[0].Value.ToString())
                        {
                            CreateForm createForm = new CreateForm(this, country, CountriesPath);
                            createForm.Text = "Редагувати країну";
                            createForm.Show();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Таблиця країн пуста, країн у файлі, що зчитується, немає", "Помилка!", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        private void updateToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            UpdateCountry();
        }

        private void contextUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateCountry();
        }
        
        private void DeleteCountry()
        {
            if (Countries != null)
            {
                if (dataGridViewCountries.SelectedRows.Count > 0)
                {
                    foreach (Country country in Countries)
                    {
                        if (country.Name == dataGridViewCountries.SelectedRows[0].Cells[0].Value.ToString())
                        {
                            Countries.Remove(country);
                            GridRefresh();
                            using (BinaryWriter writer = new BinaryWriter(File.Create(CountriesPath)))
                            {
                                Country.WriteCountries(writer, Countries);
                            }
                            return;
                        }
                    }
                }
                else if (dataGridViewCountries.SelectedCells.Count > 0)
                {
                    int row = dataGridViewCountries.SelectedCells[0].RowIndex;
                    foreach (Country country in Countries)
                    {
                        if (country.Name == dataGridViewCountries.Rows[row].Cells[0].Value.ToString())
                        {
                            Countries.Remove(country);
                            GridRefresh();
                            using (BinaryWriter writer = new BinaryWriter(File.Create(CountriesPath)))
                            {
                                Country.WriteCountries(writer, Countries);
                            }
                            return;
                        }
                    }
                }

            }
            else
            {
                MessageBox.Show("Таблиця країн пуста, країн у файлі, що зчитується, немає", "Помилка!", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteCountry();
        }

        private void contextDeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteCountry();
        }
        private void continentToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridRefresh();
        }

        private void areaToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridRefresh();
        }

        private void aimToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridRefresh();
        }

        private void toolStripTextBox_TextChanged(object sender, EventArgs e)
        {
            GridRefresh();
        }
    }
}

