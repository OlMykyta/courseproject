﻿using Information;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace CourseForm
{
    public partial class CreateForm : Form
    {
        Country Country = null;
        string Path;
        CountriesForm Form;
        public CreateForm(CountriesForm form, string path)
        {
            InitializeComponent();
            Path = path;
            comboBoxContinent.SelectedIndex = 0;
            comboBoxArea.SelectedIndex = 0;
            buttonDo.Text = "Створити";
            Form = form;
        }
        public CreateForm(CountriesForm form, Country country, string path)
        {
            InitializeComponent();
            Path = path;
            textBoxName.Text = country.Name;
            numericUpDownPopulation.Value = country.Population;
            numericUpDownGDP.Value = (decimal)country.GDP;
            comboBoxContinent.SelectedIndex = country.Continent;
            comboBoxArea.SelectedIndex = country.Side;
            Country = country;
            buttonDo.Text = "Редагувати";
            Form = form;
            checkBoxDontClose.Visible = false;
            checkBoxDontClose.Checked = false;
        }

        private void buttonDo_Click(object sender, EventArgs e)
        {
            Country newCountry = new Country(textBoxName.Text, (uint)numericUpDownPopulation.Value, (float)numericUpDownGDP.Value,
                        (short)comboBoxContinent.SelectedIndex, (short)comboBoxArea.SelectedIndex);
            if (!Country.CheckCountry(newCountry))
            {
                MessageBox.Show("Країна містить помилку(-и). Будь-ласка, використовуйте українські літери та дотриуйтесь " +
                    "правил орфографії.", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            List<Country> countries;
            if (Country == null)
            {
                using (BinaryReader reader = new BinaryReader(File.OpenRead(Path)))
                {
                    countries = Country.GetCountries(reader);
                }
                if (countries == null)
                {
                    countries = new List<Country>();
                    countries.Add(newCountry);
                }
                else
                {
                    foreach(Country country in countries)
                    {
                        if(Country.Compare(country, newCountry))
                        {
                            MessageBox.Show("Країни не можуть мати однакові назви.", "Помилка!", MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    countries.Add(newCountry);
                }
                using (BinaryWriter writer = new BinaryWriter(File.Create(Path)))
                {
                    Country.WriteCountries(writer, countries);
                }
            }
            else
            {
                using (BinaryReader reader = new BinaryReader(File.Open(Path, FileMode.OpenOrCreate)))
                {
                    countries = Country.GetCountries(reader);
                }
                for (int i = 0; i < countries.Count; i++)
                {
                    if (Country.Compare(countries[i], Country))
                    {
                        countries[i] = new Country(textBoxName.Text, (uint)numericUpDownPopulation.Value, (float)numericUpDownGDP.Value,
                    (short)comboBoxContinent.SelectedIndex, (short)comboBoxArea.SelectedIndex);
                        break;
                    }
                }
                using (BinaryWriter writer = new BinaryWriter(File.Create(Path)))
                {
                    Country.WriteCountries(writer, countries);
                }
            }
            Form.SetCountries(countries);
            if (!checkBoxDontClose.Checked)
            {
                Close();
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxName.Clear();
            numericUpDownPopulation.Value = numericUpDownPopulation.Minimum;
            numericUpDownGDP.Value = numericUpDownGDP.Minimum;
            comboBoxContinent.SelectedIndex = 0;
            comboBoxArea.SelectedIndex = 0;
        }
    }
}
