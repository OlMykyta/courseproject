﻿namespace CourseForm
{
    partial class CreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateForm));
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.comboBoxContinent = new System.Windows.Forms.ComboBox();
            this.comboBoxArea = new System.Windows.Forms.ComboBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelPopulation = new System.Windows.Forms.Label();
            this.labelGDP = new System.Windows.Forms.Label();
            this.labelContinent = new System.Windows.Forms.Label();
            this.labelArea = new System.Windows.Forms.Label();
            this.numericUpDownPopulation = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownGDP = new System.Windows.Forms.NumericUpDown();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonDo = new System.Windows.Forms.Button();
            this.checkBoxDontClose = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPopulation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGDP)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(12, 70);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(560, 29);
            this.textBoxName.TabIndex = 0;
            this.textBoxName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // comboBoxContinent
            // 
            this.comboBoxContinent.FormattingEnabled = true;
            this.comboBoxContinent.Items.AddRange(new object[] {
            "Європа",
            "Азія",
            "Африка",
            "Північна Америка",
            "Південна Америка",
            "Австралія та Океанія"});
            this.comboBoxContinent.Location = new System.Drawing.Point(12, 184);
            this.comboBoxContinent.Name = "comboBoxContinent";
            this.comboBoxContinent.Size = new System.Drawing.Size(277, 30);
            this.comboBoxContinent.TabIndex = 4;
            // 
            // comboBoxArea
            // 
            this.comboBoxArea.FormattingEnabled = true;
            this.comboBoxArea.Items.AddRange(new object[] {
            "Північ",
            "Південь",
            "Захід",
            "Схід",
            "Центр"});
            this.comboBoxArea.Location = new System.Drawing.Point(295, 184);
            this.comboBoxArea.Name = "comboBoxArea";
            this.comboBoxArea.Size = new System.Drawing.Size(277, 30);
            this.comboBoxArea.TabIndex = 5;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(231, 45);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(122, 22);
            this.labelName.TabIndex = 5;
            this.labelName.Text = "Назва країни";
            // 
            // labelPopulation
            // 
            this.labelPopulation.AutoSize = true;
            this.labelPopulation.Location = new System.Drawing.Point(45, 102);
            this.labelPopulation.Name = "labelPopulation";
            this.labelPopulation.Size = new System.Drawing.Size(213, 22);
            this.labelPopulation.TabIndex = 6;
            this.labelPopulation.Text = "Населення (ціле число)";
            // 
            // labelGDP
            // 
            this.labelGDP.AutoSize = true;
            this.labelGDP.Location = new System.Drawing.Point(298, 102);
            this.labelGDP.Name = "labelGDP";
            this.labelGDP.Size = new System.Drawing.Size(269, 22);
            this.labelGDP.TabIndex = 7;
            this.labelGDP.Text = "ВВП (дробове число, млрд $)";
            // 
            // labelContinent
            // 
            this.labelContinent.AutoSize = true;
            this.labelContinent.Location = new System.Drawing.Point(102, 159);
            this.labelContinent.Name = "labelContinent";
            this.labelContinent.Size = new System.Drawing.Size(99, 22);
            this.labelContinent.TabIndex = 8;
            this.labelContinent.Text = "Континент";
            // 
            // labelArea
            // 
            this.labelArea.AutoSize = true;
            this.labelArea.Location = new System.Drawing.Point(348, 159);
            this.labelArea.Name = "labelArea";
            this.labelArea.Size = new System.Drawing.Size(165, 22);
            this.labelArea.TabIndex = 9;
            this.labelArea.Text = "Регіон континенту";
            // 
            // numericUpDownPopulation
            // 
            this.numericUpDownPopulation.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDownPopulation.Location = new System.Drawing.Point(12, 127);
            this.numericUpDownPopulation.Maximum = new decimal(new int[] {
            -294967296,
            0,
            0,
            0});
            this.numericUpDownPopulation.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDownPopulation.Name = "numericUpDownPopulation";
            this.numericUpDownPopulation.Size = new System.Drawing.Size(277, 29);
            this.numericUpDownPopulation.TabIndex = 1;
            this.numericUpDownPopulation.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownPopulation.ThousandsSeparator = true;
            this.numericUpDownPopulation.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // numericUpDownGDP
            // 
            this.numericUpDownGDP.DecimalPlaces = 3;
            this.numericUpDownGDP.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownGDP.Location = new System.Drawing.Point(295, 127);
            this.numericUpDownGDP.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.numericUpDownGDP.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericUpDownGDP.Name = "numericUpDownGDP";
            this.numericUpDownGDP.Size = new System.Drawing.Size(277, 29);
            this.numericUpDownGDP.TabIndex = 2;
            this.numericUpDownGDP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownGDP.ThousandsSeparator = true;
            this.numericUpDownGDP.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(12, 313);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(560, 36);
            this.buttonClear.TabIndex = 8;
            this.buttonClear.Text = "Стерти";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonDo
            // 
            this.buttonDo.Location = new System.Drawing.Point(12, 271);
            this.buttonDo.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonDo.Name = "buttonDo";
            this.buttonDo.Size = new System.Drawing.Size(560, 36);
            this.buttonDo.TabIndex = 7;
            this.buttonDo.Text = "Створити/Редагувати";
            this.buttonDo.UseVisualStyleBackColor = true;
            this.buttonDo.Click += new System.EventHandler(this.buttonDo_Click);
            // 
            // checkBoxDontClose
            // 
            this.checkBoxDontClose.AutoSize = true;
            this.checkBoxDontClose.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxDontClose.Location = new System.Drawing.Point(217, 245);
            this.checkBoxDontClose.Name = "checkBoxDontClose";
            this.checkBoxDontClose.Size = new System.Drawing.Size(147, 20);
            this.checkBoxDontClose.TabIndex = 6;
            this.checkBoxDontClose.Text = "не закривати форму";
            this.checkBoxDontClose.UseVisualStyleBackColor = true;
            // 
            // CreateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.checkBoxDontClose);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonDo);
            this.Controls.Add(this.numericUpDownGDP);
            this.Controls.Add(this.numericUpDownPopulation);
            this.Controls.Add(this.labelArea);
            this.Controls.Add(this.labelContinent);
            this.Controls.Add(this.labelGDP);
            this.Controls.Add(this.labelPopulation);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.comboBoxArea);
            this.Controls.Add(this.comboBoxContinent);
            this.Controls.Add(this.textBoxName);
            this.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Створення/Редагування форми";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPopulation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGDP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.ComboBox comboBoxContinent;
        private System.Windows.Forms.ComboBox comboBoxArea;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelPopulation;
        private System.Windows.Forms.Label labelGDP;
        private System.Windows.Forms.Label labelContinent;
        private System.Windows.Forms.Label labelArea;
        private System.Windows.Forms.NumericUpDown numericUpDownPopulation;
        private System.Windows.Forms.NumericUpDown numericUpDownGDP;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonDo;
        private System.Windows.Forms.CheckBox checkBoxDontClose;
    }
}