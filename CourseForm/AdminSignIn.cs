﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Information;

namespace CourseForm
{
    public partial class AdminSignIn : Form
    {
        string Path;
        CountriesForm Primary;
        public AdminSignIn(string path, CountriesForm primary)
        {
            InitializeComponent();
            Path = path;
            Primary = primary;
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            string login = textBoxLogIn.Text;
            string password = textBoxPassword.Text;
            string repPassword = textBoxRepPassword.Text;
            string error = "";
            bool correct = true;
            List<Account> accounts = null;

            using(BinaryReader reader = new BinaryReader(File.Open(Path, FileMode.OpenOrCreate)))
            {
                try { 
                    accounts = Account.GetAccounts(reader);
                    correct = Account.CheckSignIn(accounts, login, password, repPassword, out error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    accounts = null;
                    correct = true;
                }
                reader.Close();
            }

            if(correct)
            {
                if(accounts != null)
                {
                    accounts.Add(new Account(login, password));
                    using(BinaryWriter writer = new BinaryWriter(File.Open(Path, FileMode.Create)))
                    {
                        Account.WriteAccounts(writer, accounts);
                        writer.Close();
                    }
                }
                else
                {
                    using(BinaryWriter writer = new BinaryWriter(File.Open(Path, FileMode.Create)))
                    {
                        writer.Write(1);
                        writer.Write(login);
                        writer.Write(password);
                        writer.Flush();
                        writer.Close();
                    }
                }
                AdminLogIn adminLogIn = new AdminLogIn(Primary, Path);
                adminLogIn.Show();
                Close();
            }
            else
            {
                MessageBox.Show(error, "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void checkBoxShowPass_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxShowPass.Checked)
            {
                textBoxPassword.UseSystemPasswordChar = false;
                textBoxRepPassword.UseSystemPasswordChar = false;
            }
            else
            {
                textBoxPassword.UseSystemPasswordChar = true;
                textBoxRepPassword.UseSystemPasswordChar = true;
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxLogIn.Clear();
            textBoxPassword.Clear();
            textBoxRepPassword.Clear();
        }

        private void textBoxLogIn_TextChanged(object sender, EventArgs e)
        {
            int count = textBoxLogIn.Text.Length;
            if (count < 5)
            {
                labelLogInCounter.ForeColor = Color.Red;
            }
            else
            {
                labelLogInCounter.ForeColor = Color.Black;
            }
            if (count != 0)
            {
                labelLogInCounter.Text = count.ToString();
            }
            else
            {
                labelLogInCounter.Text = " ";
            }
        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {
            int count = textBoxPassword.Text.Length;
            if (count < 6)
            {
                labelPasswordCounter.ForeColor = Color.Red;
            }
            else
            {
                labelPasswordCounter.ForeColor = Color.Black;
            }
            if (count != 0)
            {
                labelPasswordCounter.Text = count.ToString();
            }
            else
            {
                labelPasswordCounter.Text = " ";
            }
        }

        private void textBoxRepPassword_TextChanged(object sender, EventArgs e)
        {
            int count = textBoxRepPassword.Text.Length;
            if (count < 6)
            {
                labelRepPasswordCounter.ForeColor = Color.Red;
            }
            else
            {
                labelRepPasswordCounter.ForeColor = Color.Black;
            }
            if (count != 0)
            {
                labelRepPasswordCounter.Text = count.ToString();
            }
            else
            {
                labelRepPasswordCounter.Text = " ";
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            AdminLogIn adminLogIn = new AdminLogIn(Primary, Path);
            adminLogIn.Show();
            Close();
        }
    }
}
