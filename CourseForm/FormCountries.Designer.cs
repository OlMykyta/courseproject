﻿namespace CourseForm
{
    partial class CountriesForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CountriesForm));
            this.labelCountCountries = new System.Windows.Forms.Label();
            this.labelCounter = new System.Windows.Forms.Label();
            this.labelAccount = new System.Windows.Forms.Label();
            this.dataGridViewCountries = new System.Windows.Forms.DataGridView();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPopulation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnGDP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnContinent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnArea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextDeleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SeekModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aimModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mixedModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.continentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.continentToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.areaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.areaToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.aimParametersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aimToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.moderationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCountries)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelCountCountries
            // 
            this.labelCountCountries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCountCountries.AutoSize = true;
            this.labelCountCountries.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCountCountries.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCountCountries.Location = new System.Drawing.Point(12, 471);
            this.labelCountCountries.Name = "labelCountCountries";
            this.labelCountCountries.Size = new System.Drawing.Size(140, 21);
            this.labelCountCountries.TabIndex = 1;
            this.labelCountCountries.Text = "Всього країн:";
            // 
            // labelCounter
            // 
            this.labelCounter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCounter.AutoSize = true;
            this.labelCounter.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCounter.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelCounter.Location = new System.Drawing.Point(158, 471);
            this.labelCounter.Name = "labelCounter";
            this.labelCounter.Size = new System.Drawing.Size(28, 21);
            this.labelCounter.TabIndex = 2;
            this.labelCounter.Text = "   ";
            // 
            // labelAccount
            // 
            this.labelAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAccount.AutoSize = true;
            this.labelAccount.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAccount.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelAccount.Location = new System.Drawing.Point(429, 471);
            this.labelAccount.Name = "labelAccount";
            this.labelAccount.Size = new System.Drawing.Size(343, 21);
            this.labelAccount.TabIndex = 19;
            this.labelAccount.Text = "Адміністративні функції вимкнені";
            this.labelAccount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dataGridViewCountries
            // 
            this.dataGridViewCountries.AllowUserToAddRows = false;
            this.dataGridViewCountries.AllowUserToDeleteRows = false;
            this.dataGridViewCountries.AllowUserToResizeRows = false;
            this.dataGridViewCountries.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewCountries.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewCountries.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewCountries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCountries.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnName,
            this.ColumnPopulation,
            this.ColumnGDP,
            this.ColumnContinent,
            this.ColumnArea});
            this.dataGridViewCountries.ContextMenuStrip = this.contextMenuStrip;
            this.dataGridViewCountries.Location = new System.Drawing.Point(0, 24);
            this.dataGridViewCountries.MultiSelect = false;
            this.dataGridViewCountries.Name = "dataGridViewCountries";
            this.dataGridViewCountries.ReadOnly = true;
            this.dataGridViewCountries.Size = new System.Drawing.Size(784, 444);
            this.dataGridViewCountries.TabIndex = 21;
            // 
            // ColumnName
            // 
            this.ColumnName.HeaderText = "Назва";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.ReadOnly = true;
            // 
            // ColumnPopulation
            // 
            this.ColumnPopulation.HeaderText = "Населення";
            this.ColumnPopulation.Name = "ColumnPopulation";
            this.ColumnPopulation.ReadOnly = true;
            // 
            // ColumnGDP
            // 
            this.ColumnGDP.HeaderText = "ВВП (млрд. $)";
            this.ColumnGDP.Name = "ColumnGDP";
            this.ColumnGDP.ReadOnly = true;
            // 
            // ColumnContinent
            // 
            this.ColumnContinent.HeaderText = "Континент";
            this.ColumnContinent.Name = "ColumnContinent";
            this.ColumnContinent.ReadOnly = true;
            // 
            // ColumnArea
            // 
            this.ColumnArea.HeaderText = "Регіон континенту";
            this.ColumnArea.Name = "ColumnArea";
            this.ColumnArea.ReadOnly = true;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Enabled = false;
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextAddToolStripMenuItem,
            this.contextUpdateToolStripMenuItem,
            this.contextDeleteToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(173, 70);
            // 
            // contextAddToolStripMenuItem
            // 
            this.contextAddToolStripMenuItem.Enabled = false;
            this.contextAddToolStripMenuItem.Name = "contextAddToolStripMenuItem";
            this.contextAddToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.contextAddToolStripMenuItem.Text = "Додати країну";
            this.contextAddToolStripMenuItem.Click += new System.EventHandler(this.contextAddToolStripMenuItem_Click);
            // 
            // contextUpdateToolStripMenuItem
            // 
            this.contextUpdateToolStripMenuItem.Enabled = false;
            this.contextUpdateToolStripMenuItem.Name = "contextUpdateToolStripMenuItem";
            this.contextUpdateToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.contextUpdateToolStripMenuItem.Text = "Редагувати країну";
            this.contextUpdateToolStripMenuItem.Click += new System.EventHandler(this.contextUpdateToolStripMenuItem_Click);
            // 
            // contextDeleteToolStripMenuItem
            // 
            this.contextDeleteToolStripMenuItem.Enabled = false;
            this.contextDeleteToolStripMenuItem.Name = "contextDeleteToolStripMenuItem";
            this.contextDeleteToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.contextDeleteToolStripMenuItem.Text = "Видалити країну";
            this.contextDeleteToolStripMenuItem.Click += new System.EventHandler(this.contextDeleteToolStripMenuItem_Click);
            // 
            // SeekModeToolStripMenuItem
            // 
            this.SeekModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generalModeToolStripMenuItem,
            this.aimModeToolStripMenuItem,
            this.mixedModeToolStripMenuItem,
            this.toolStripSeparator,
            this.continentToolStripMenuItem,
            this.areaToolStripMenuItem,
            this.aimParametersToolStripMenuItem});
            this.SeekModeToolStripMenuItem.Name = "SeekModeToolStripMenuItem";
            this.SeekModeToolStripMenuItem.Size = new System.Drawing.Size(103, 20);
            this.SeekModeToolStripMenuItem.Text = "Режим пошуку";
            // 
            // generalModeToolStripMenuItem
            // 
            this.generalModeToolStripMenuItem.Name = "generalModeToolStripMenuItem";
            this.generalModeToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.generalModeToolStripMenuItem.Text = "Приблизний пошук";
            this.generalModeToolStripMenuItem.Click += new System.EventHandler(this.generalModeToolStripMenuItem_Click);
            // 
            // aimModeToolStripMenuItem
            // 
            this.aimModeToolStripMenuItem.Name = "aimModeToolStripMenuItem";
            this.aimModeToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.aimModeToolStripMenuItem.Text = "Точний пошук";
            this.aimModeToolStripMenuItem.Click += new System.EventHandler(this.aimModeToolStripMenuItem_Click);
            // 
            // mixedModeToolStripMenuItem
            // 
            this.mixedModeToolStripMenuItem.Name = "mixedModeToolStripMenuItem";
            this.mixedModeToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.mixedModeToolStripMenuItem.Text = "Змішаний пошук";
            this.mixedModeToolStripMenuItem.Click += new System.EventHandler(this.mixedModeToolStripMenuItem_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(230, 6);
            this.toolStripSeparator.Visible = false;
            // 
            // continentToolStripMenuItem
            // 
            this.continentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.continentToolStripComboBox});
            this.continentToolStripMenuItem.Name = "continentToolStripMenuItem";
            this.continentToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.continentToolStripMenuItem.Text = "Континент";
            this.continentToolStripMenuItem.Visible = false;
            // 
            // continentToolStripComboBox
            // 
            this.continentToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.continentToolStripComboBox.Items.AddRange(new object[] {
            "Всі",
            "Європа",
            "Азія",
            "Африка",
            "Північна Америка",
            "Південна Америка",
            "Австралія та Океанія"});
            this.continentToolStripComboBox.Name = "continentToolStripComboBox";
            this.continentToolStripComboBox.Size = new System.Drawing.Size(150, 23);
            this.continentToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.continentToolStripComboBox_SelectedIndexChanged);
            // 
            // areaToolStripMenuItem
            // 
            this.areaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.areaToolStripComboBox});
            this.areaToolStripMenuItem.Name = "areaToolStripMenuItem";
            this.areaToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.areaToolStripMenuItem.Text = "Регіон континенту";
            this.areaToolStripMenuItem.Visible = false;
            // 
            // areaToolStripComboBox
            // 
            this.areaToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.areaToolStripComboBox.Items.AddRange(new object[] {
            "Всі",
            "Північ",
            "Південь",
            "Захід",
            "Схід",
            "Центр"});
            this.areaToolStripComboBox.Name = "areaToolStripComboBox";
            this.areaToolStripComboBox.Size = new System.Drawing.Size(100, 23);
            this.areaToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.areaToolStripComboBox_SelectedIndexChanged);
            // 
            // aimParametersToolStripMenuItem
            // 
            this.aimParametersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aimToolStripComboBox,
            this.toolStripTextBox});
            this.aimParametersToolStripMenuItem.Name = "aimParametersToolStripMenuItem";
            this.aimParametersToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.aimParametersToolStripMenuItem.Text = "Параметри  точного пошуку";
            this.aimParametersToolStripMenuItem.Visible = false;
            // 
            // aimToolStripComboBox
            // 
            this.aimToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.aimToolStripComboBox.Items.AddRange(new object[] {
            "За назвою / частиною",
            "За населенням (>=)",
            "За населенням (<=)",
            "За ВВП (>=)",
            "За ВВП (<=)"});
            this.aimToolStripComboBox.Name = "aimToolStripComboBox";
            this.aimToolStripComboBox.Size = new System.Drawing.Size(150, 23);
            this.aimToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.aimToolStripComboBox_SelectedIndexChanged);
            // 
            // toolStripTextBox
            // 
            this.toolStripTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripTextBox.Name = "toolStripTextBox";
            this.toolStripTextBox.Size = new System.Drawing.Size(150, 23);
            this.toolStripTextBox.TextChanged += new System.EventHandler(this.toolStripTextBox_TextChanged);
            // 
            // moderationToolStripMenuItem
            // 
            this.moderationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.updateToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.loginToolStripMenuItem});
            this.moderationToolStripMenuItem.Name = "moderationToolStripMenuItem";
            this.moderationToolStripMenuItem.Size = new System.Drawing.Size(158, 20);
            this.moderationToolStripMenuItem.Text = "Адміністративний доступ";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addToolStripMenuItem.Text = "Додати країну";
            this.addToolStripMenuItem.Visible = false;
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // updateToolStripMenuItem
            // 
            this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
            this.updateToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.updateToolStripMenuItem.Text = "Редагувати країну";
            this.updateToolStripMenuItem.Visible = false;
            this.updateToolStripMenuItem.Click += new System.EventHandler(this.updateToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.deleteToolStripMenuItem.Text = "Видалити країну";
            this.deleteToolStripMenuItem.Visible = false;
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.loginToolStripMenuItem.Text = "Увійти";
            this.loginToolStripMenuItem.Click += new System.EventHandler(this.loginToolStripMenuItem_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SeekModeToolStripMenuItem,
            this.moderationToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(784, 24);
            this.menuStrip.TabIndex = 20;
            this.menuStrip.Text = "menuStrip";
            // 
            // CountriesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Firebrick;
            this.ClientSize = new System.Drawing.Size(784, 501);
            this.Controls.Add(this.dataGridViewCountries);
            this.Controls.Add(this.labelAccount);
            this.Controls.Add(this.labelCounter);
            this.Controls.Add(this.labelCountCountries);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MinimumSize = new System.Drawing.Size(800, 540);
            this.Name = "CountriesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Пошукова система країн світу";
            this.Load += new System.EventHandler(this.FormCountries_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCountries)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelCountCountries;
        private System.Windows.Forms.Label labelCounter;
        private System.Windows.Forms.Label labelAccount;
        private System.Windows.Forms.DataGridView dataGridViewCountries;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPopulation;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnGDP;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnContinent;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnArea;
        private System.Windows.Forms.ToolStripMenuItem SeekModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generalModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aimModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mixedModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem continentToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox continentToolStripComboBox;
        private System.Windows.Forms.ToolStripMenuItem areaToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox areaToolStripComboBox;
        private System.Windows.Forms.ToolStripMenuItem aimParametersToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox aimToolStripComboBox;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox;
        private System.Windows.Forms.ToolStripMenuItem moderationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem contextAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contextUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contextDeleteToolStripMenuItem;
    }
}

