﻿namespace CourseForm
{
    partial class AdminSignIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminSignIn));
            this.labelLogin = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.textBoxLogIn = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.textBoxRepPassword = new System.Windows.Forms.TextBox();
            this.labelRepPassword = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.checkBoxShowPass = new System.Windows.Forms.CheckBox();
            this.labelLogInCounter = new System.Windows.Forms.Label();
            this.labelPasswordCounter = new System.Windows.Forms.Label();
            this.labelRepPasswordCounter = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Location = new System.Drawing.Point(130, 27);
            this.labelLogin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(60, 23);
            this.labelLogin.TabIndex = 0;
            this.labelLogin.Text = "Логін";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(115, 65);
            this.labelPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(75, 23);
            this.labelPassword.TabIndex = 1;
            this.labelPassword.Text = "Пароль";
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(13, 179);
            this.buttonCreate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(458, 36);
            this.buttonCreate.TabIndex = 4;
            this.buttonCreate.Text = "Створити";
            this.buttonCreate.UseVisualStyleBackColor = true;
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // textBoxLogIn
            // 
            this.textBoxLogIn.Location = new System.Drawing.Point(220, 24);
            this.textBoxLogIn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxLogIn.Name = "textBoxLogIn";
            this.textBoxLogIn.Size = new System.Drawing.Size(190, 32);
            this.textBoxLogIn.TabIndex = 0;
            this.textBoxLogIn.TextChanged += new System.EventHandler(this.textBoxLogIn_TextChanged);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(220, 62);
            this.textBoxPassword.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(190, 32);
            this.textBoxPassword.TabIndex = 1;
            this.textBoxPassword.UseSystemPasswordChar = true;
            this.textBoxPassword.TextChanged += new System.EventHandler(this.textBoxPassword_TextChanged);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(13, 221);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(458, 36);
            this.buttonClear.TabIndex = 5;
            this.buttonClear.Text = "Стерти";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // textBoxRepPassword
            // 
            this.textBoxRepPassword.Location = new System.Drawing.Point(220, 100);
            this.textBoxRepPassword.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxRepPassword.Name = "textBoxRepPassword";
            this.textBoxRepPassword.Size = new System.Drawing.Size(190, 32);
            this.textBoxRepPassword.TabIndex = 2;
            this.textBoxRepPassword.UseSystemPasswordChar = true;
            this.textBoxRepPassword.TextChanged += new System.EventHandler(this.textBoxRepPassword_TextChanged);
            // 
            // labelRepPassword
            // 
            this.labelRepPassword.AutoSize = true;
            this.labelRepPassword.Location = new System.Drawing.Point(24, 103);
            this.labelRepPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelRepPassword.Name = "labelRepPassword";
            this.labelRepPassword.Size = new System.Drawing.Size(166, 23);
            this.labelRepPassword.TabIndex = 2;
            this.labelRepPassword.Text = "Повторіть пароль";
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(13, 263);
            this.buttonBack.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(458, 36);
            this.buttonBack.TabIndex = 6;
            this.buttonBack.Text = "Повернутися до входу";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // checkBoxShowPass
            // 
            this.checkBoxShowPass.AutoSize = true;
            this.checkBoxShowPass.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxShowPass.Location = new System.Drawing.Point(220, 138);
            this.checkBoxShowPass.Name = "checkBoxShowPass";
            this.checkBoxShowPass.Size = new System.Drawing.Size(132, 21);
            this.checkBoxShowPass.TabIndex = 3;
            this.checkBoxShowPass.Text = "Показати пароль";
            this.checkBoxShowPass.UseVisualStyleBackColor = true;
            this.checkBoxShowPass.CheckedChanged += new System.EventHandler(this.checkBoxShowPass_CheckedChanged);
            // 
            // labelLogInCounter
            // 
            this.labelLogInCounter.AutoSize = true;
            this.labelLogInCounter.Location = new System.Drawing.Point(417, 27);
            this.labelLogInCounter.Name = "labelLogInCounter";
            this.labelLogInCounter.Size = new System.Drawing.Size(15, 23);
            this.labelLogInCounter.TabIndex = 7;
            this.labelLogInCounter.Text = " ";
            // 
            // labelPasswordCounter
            // 
            this.labelPasswordCounter.AutoSize = true;
            this.labelPasswordCounter.Location = new System.Drawing.Point(417, 65);
            this.labelPasswordCounter.Name = "labelPasswordCounter";
            this.labelPasswordCounter.Size = new System.Drawing.Size(15, 23);
            this.labelPasswordCounter.TabIndex = 8;
            this.labelPasswordCounter.Text = " ";
            // 
            // labelRepPasswordCounter
            // 
            this.labelRepPasswordCounter.AutoSize = true;
            this.labelRepPasswordCounter.Location = new System.Drawing.Point(417, 103);
            this.labelRepPasswordCounter.Name = "labelRepPasswordCounter";
            this.labelRepPasswordCounter.Size = new System.Drawing.Size(15, 23);
            this.labelRepPasswordCounter.TabIndex = 9;
            this.labelRepPasswordCounter.Text = " ";
            // 
            // AdminSignIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 311);
            this.Controls.Add(this.labelRepPasswordCounter);
            this.Controls.Add(this.labelPasswordCounter);
            this.Controls.Add(this.labelLogInCounter);
            this.Controls.Add(this.checkBoxShowPass);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.textBoxRepPassword);
            this.Controls.Add(this.labelRepPassword);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxLogIn);
            this.Controls.Add(this.buttonCreate);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelLogin);
            this.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AdminSignIn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Доступ до адміністративних можливостей";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Button buttonCreate;
        private System.Windows.Forms.TextBox textBoxLogIn;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.TextBox textBoxRepPassword;
        private System.Windows.Forms.Label labelRepPassword;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.CheckBox checkBoxShowPass;
        private System.Windows.Forms.Label labelLogInCounter;
        private System.Windows.Forms.Label labelPasswordCounter;
        private System.Windows.Forms.Label labelRepPasswordCounter;
    }
}