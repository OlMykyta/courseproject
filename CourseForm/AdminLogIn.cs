﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Information;

namespace CourseForm
{
    public partial class AdminLogIn : Form
    {
        CountriesForm PrimaryForm;
        string Path;


        public AdminLogIn()
        {
            InitializeComponent();
        }
        public AdminLogIn(string path)
        {
            InitializeComponent();
            Path = path;
        }
        public AdminLogIn(CountriesForm form, string path)
        {
            InitializeComponent();
            PrimaryForm = form;
            Path = path;
        }

        private void buttonLogin_Click(object sender, System.EventArgs e)
        {
            string logIn = "" + textBoxLogIn.Text;
            string password = textBoxPassword.Text;
            if(logIn == "" || password == null)
            {
                MessageBox.Show("Введіть дані");
                return;
            }

            List<Account> accounts = null;
            using (BinaryReader reader = new BinaryReader(File.OpenRead(Path)))
            {
                if(reader.PeekChar() == -1)
                {
                    MessageBox.Show("Акаунтів не знайдено", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    accounts = Account.GetAccounts(reader);
                }

            }

            bool correct = false;
            if (accounts != null)
            {
                string alert;
                bool admin;
                Account account;
                correct = Account.CheckLogIn(accounts, logIn, password, out alert, out admin, out account);
                if(correct)
                {
                    PrimaryForm.SetAccount(admin, account);
                    Close();
                }
                else
                {
                    MessageBox.Show(alert, "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void buttonCreateNew_Click(object sender, System.EventArgs e)
        {
            AdminSignIn adminSignIn = new AdminSignIn(Path, PrimaryForm);
            adminSignIn.Show();
            Close();
        }

        private void checkBoxShowPass_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxShowPass.Checked)
            {
                textBoxPassword.UseSystemPasswordChar = false;
            }
            else
            {
                textBoxPassword.UseSystemPasswordChar = true;
            }
        }

        private void textBoxLogIn_TextChanged(object sender, System.EventArgs e)
        {
            int count = textBoxLogIn.Text.Length;
            if(count < 5)
            {
                labelLogInCounter.ForeColor = Color.Red;
            }
            else
            {
                labelLogInCounter.ForeColor = Color.Black;
            }
            if (count != 0)
            {
                labelLogInCounter.Text = count.ToString();
            }
            else
            {
                labelLogInCounter.Text = " ";
            }
        }

        private void textBoxPassword_TextChanged(object sender, System.EventArgs e)
        {
            int count = textBoxPassword.Text.Length;
            if (count < 6)
            {
                labelPasswordCounter.ForeColor = Color.Red;
            }
            else
            {
                labelPasswordCounter.ForeColor = Color.Black;
            }
            if (count != 0)
            {
                labelPasswordCounter.Text = count.ToString();
            }
            else
            {
                labelPasswordCounter.Text = " ";
            }
        }
    }
}
